const canvas = new DrawableCanvas(document.getElementById('canvas'));
        const learn = document.getElementById('learn');
        const input = document.getElementById('name');
        const neuronBtn = document.getElementById('neuronBtn');
        const clear = document.getElementById('clear');
        const addData = document.getElementById('addData');
        const output = document.getElementById('output');
        const likelyOutput = document.getElementById('likelyOutput');
        const devOutput = document.getElementById('devOutput');
        const trainOutput = document.getElementById('trainOutput');
        const logList = document.getElementById('logList');

        let logData = '';
        let name = '';
        let dataCount = -1;
        let isLearn = false;
        
        input.addEventListener('keyup', () => {
            name = input.value;
            console.log(name)
        })

        const config = {
            binaryThresh: 0.5, // ¯\_(ツ)_/¯
            hiddenLayers: [], // array of ints 
            activation: 'sigmoid' 
        };

        const net = new brain.NeuralNetwork();

        let data = [];

        addData.addEventListener('click', () => {
            let out = {[name]: 1};
            if(name.length){
            dataCount++;
            data.push({ input: canvas.getVector(), output: out })
            addLogData(data,1);
            } else {
                devOut.innerHTML = 'Name your image!';
            }

        }, false);

        learn.addEventListener('click', () => {
            let trainResult = net.train(data);
            if(trainOutput){
                trainOutput.innerHTML = `Error: ${trainResult.error}
                iterations: ${trainResult.iterations}`;
                addLogData(data, 2);
                isLearn = true;
            }
        }, false)

        neuronBtn.addEventListener('click', () => {
            if (isLearn) {
                let devData = '';

            const result = net.run(canvas.getVector())
            
            let names = Object.keys(result);
            let probabilities = Object.values(result);

            for(let i = 0; i < names.length; i++){
                
                let probability = Math.round(Number(probabilities[i]).toPrecision(2) * 100000) / 1000;
                devData += `Name: "${names[i]}"  Probability: ${probability} %</br>`;
            }

            if(result) {
                likelyOutput.innerHTML = `${likely(result)}`;
                devOutput.innerHTML = `${devData}`;
            }

            addLogData(0, 4);
            console.log(result);
            console.log(likely(result));
            devOut(result);

            } else {
                alert('learn AI first');
            }
        })

        clear.addEventListener('click', () => {
            addLogData(0, 3);
            canvas.reset();
        }) 
        
        //algorithm return name of the most likely name
        const likely = (r) => {
            let names = Object.keys(r);
            let values = Object.values(r);
            
            let max = values[0];
            let n = 0;
            for (let i = 0; i < values.length; i++){
                if (max < values[i]){
                    n = i;
                    max = values[i];
                }
            }

            return names[n];
        }

        const devOut = r => {
            let names = Object.keys(r);
            let values = Object.values(r);

          //Why this not working
          //let data = [...names,...values].map( ([n,v], i) => {
          //return [n[i],v[i]];
          //})

            //but this work well
            
            let data = values.map((v, i) => {
                return [names[i], values[i]]; 
            })
            
            console.log(data)
            return data;
        }

        const addLogData = (data , option) => {
            //option == 1 => add data with name ${x}
            //option == 2 => learn! 
            //option == 3 => clear canvas

            let time = new Date().toLocaleTimeString();

            switch (option){
                case 1: {
                    logData += `<li> ${time}  Add paint with name: <b>${Object.keys(data[dataCount].output)}</b></li>`;
                } break;
                case 2: {
                    logData += `<li> ${time}  You learn AI with ${Object.keys(data).length} images</li>`;
                } break;
                case 3: {
                    logData += `<li> ${time}  You cleaned the canvas </li> `;
                } break;
                case 4: {
                    logData += `<li> ${time} <b>Neural network is working </b></li>`;
                } break; 
                default: {
                    console.log('default')
                }

            }
            logList.innerHTML = logData;
            console.log(logData);
        }