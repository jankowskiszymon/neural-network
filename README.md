# Neural-network demo

Live demo wersion: [demo](http://jankowskiszymon.pl/neural-network/) Enjoy!

## About

This is a simple neural network demo, that learns images and then describes them. Remember that before artificial intelligence will guess, you have to first teach AI images and what they are called. 

The project is written with [brain.js](https://brain.js.org/#/) and vanillaJS. 

## How to use it

- [1] Draw an image
- [2] Name it! 
- [3] Draw another image
- [4] Learn AI 
- [5] Check network
- [6] Enjoy! 

## Licence 

Completely MIT Licensed. Including ALL dependencies.
